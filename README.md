# Procédure pour utiliser le bras de A à Z

## Branchements
1) plug HUB phidget, Arduino mega, Joy aux ports USB de la rasp, prise HDMI entre écran et la rasp
2) brancher la raspberry au secteur et l'allumer

## Récupération des dernières modifications sur les programmes
Sur la raspberry :
```
cd ros2_ws/src/arm-firmware-2022-2023/
git checkout main
git pull
cd ..
cd arm-os-2022-2023/
git checkout develop 
git pull
cd ~/ros2_ws/
source /opt/ros/foxy/setup.bash
colcon build
source install/setup.bash
```

## How to launch all the cmd
1) open a terminal in the raspberry
2) in the terminal: `arduino ros2_ws/src/arm-firmware-2022-2023/arm-firmware/arm-firmware.ino`
3) in the menu Tools select "Board Arduino Mega or Mega 2560" and the good port
4) remember the value of the port displayed in the menu Tools/Port, you should have something like "/dev/ttyACM0", we will use it later
5) Upload the arduino code in the arduino mega
6) close arduino ide 
7) power supply the phidgets (put current in the electrical circuit)
8) in the terminal: `code ros2_ws/src/arm-os-2022-2023/launch_windows.bash`
9) change the ligne 3 with the correct value of the arduino mega port
10) save the file and close it
11) in the terminal: `bash ros2_ws/src/arm-os-2022-2023/launch_windows.bash`

Everything should work!

## FAQ
Errors from Phidgets: ensure all properly plugged in 
